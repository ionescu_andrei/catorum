<?php

use App\Http\Resources\Categories;
use App\Models\Category;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api', 'api.refresh-token'])->group(function () {
    Route::match(['get', 'post'], '/categories', function (Request $request) {
        return new Categories(Category::all());
    })->name('categories-list');
});

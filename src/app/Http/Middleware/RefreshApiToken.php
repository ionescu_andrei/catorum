<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class RefreshApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        $response = $next($request);

        // Perform action
        return $response->header('Authorization', 'Bearer ' . $request->user()->api_token);
    }
}

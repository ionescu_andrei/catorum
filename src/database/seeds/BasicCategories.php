<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class BasicCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = factory(Category::class, 10)->make();
        $categories->each(function ($item) {
            Category::firstOrCreate($item->toArray());
        });
    }
}

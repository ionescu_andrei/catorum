export const alert = {
    methods: {
        showAlert: function(message, type) {
            this.errors.push({
                message: message,
                type: type
            });
        },
        hideAlert: function() {
            this.errors = [];
        }
    }
};

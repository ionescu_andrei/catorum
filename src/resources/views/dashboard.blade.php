@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <ul class="list-group">
                    <li v-for="category in categories" class="list-group-item d-flex justify-content-between align-items-center">@{{ category.name }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
